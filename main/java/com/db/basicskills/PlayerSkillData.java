package com.db.basicskills;

import java.io.Serializable;

public class PlayerSkillData implements Serializable{
	private int xp;
	
	public PlayerSkillData(){
		
	}

	public PlayerSkillData(int xp) {
		super();
		this.xp = xp;
	}

	public int getXp() {
		return xp;
	}

	public void setXp(int xp) {
		this.xp = xp;
	}
	
	public void addXp(int xp){
		this.xp += xp;
	}
}
