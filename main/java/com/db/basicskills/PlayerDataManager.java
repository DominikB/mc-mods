package com.db.basicskills;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.UUID;

import net.minecraft.entity.player.EntityPlayer;

/**
 * This class manages the new data for players.
 * For example XP for the skill system.
 * @author Dominik Bruehl
 * @version 1.0
 * @date 17.02.2015
 *
 */
public abstract class PlayerDataManager {
	
	//private static final String PATH = "mods/basicSkills/playerData";
	private static final String PATH = "playerData";
	
	private static PlayerDataList pdl = new PlayerDataList();
	
	public static void loadPlayerData(){ //TODO SAVE AND LOAD ONLY SERVER SIDE!
		File file = new File(PATH);
		if(!file.exists())
			return;
		
		try {
			FileInputStream fin = new FileInputStream(file);
			@SuppressWarnings("resource")
			ObjectInputStream oos = new ObjectInputStream(fin);
			pdl = (PlayerDataList) oos.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace(); //TODO Catch better
		} catch (IOException e) {
			e.printStackTrace(); //TODO Catch better
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void savePlayerData(){ //TODO SAVE AND LOAD ONLY SERVER SIDE!
		File file = new File(PATH);
		boolean success = (file).mkdirs();
		if (!success) {
			// TODO Failed to create the folder
		}
		//if(!file.exists()){
			try {
				file.createNewFile(); //TODO Catch better
			} catch (IOException e1) {
				e1.printStackTrace(); //TODO Catch better
			}
		//}
		
		System.out.println(file.getAbsolutePath());
		
		try {
			FileOutputStream fout = new FileOutputStream(file, false);
			@SuppressWarnings("resource")
			ObjectOutputStream oos = new ObjectOutputStream(fout);		
			oos.writeObject(pdl);
		} catch (FileNotFoundException e) {
			e.printStackTrace(); //TODO Catch better
		} catch (IOException e) {
			e.printStackTrace(); //TODO Catch better
		}	
	}
	
	public static void addXPToPlayer(EntityPlayer player, int amount){
		String id = player.getGameProfile().getId().toString();
		HashMap<String, PlayerSkillData> playerdata = pdl.getPlayerdata();
		
		if(!playerdata.containsKey(id))
			playerdata.put(id, new PlayerSkillData(amount));
		else
			playerdata.get(id).addXp(amount);
		
		System.out.println("Total xp: " + playerdata.get(id).getXp());
	}
}
