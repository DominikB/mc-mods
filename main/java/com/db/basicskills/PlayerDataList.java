package com.db.basicskills;

import java.io.Serializable;
import java.util.HashMap;

public class PlayerDataList implements Serializable{

	private HashMap <String, PlayerSkillData> playerdata;
	
	public PlayerDataList(){
		playerdata = new HashMap <String, PlayerSkillData>();
	}

	public HashMap<String, PlayerSkillData> getPlayerdata() {
		return playerdata;
	}

	public void setPlayerdata(HashMap<String, PlayerSkillData> playerdata) {
		this.playerdata = playerdata;
	}
}
