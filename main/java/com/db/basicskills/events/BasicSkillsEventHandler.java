package com.db.basicskills.events;

import com.db.basicskills.PlayerDataManager;

import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.player.PlayerPickupXpEvent;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

/**
 * The basic event handler. This Handler will manage the main events
 * of the skill system.
 * @author Dominik Bruehl
 * @version 1.0
 * @date 17.02.2015
 *
 */
public class BasicSkillsEventHandler {
	@SubscribeEvent
	public void onXPAdded(PlayerPickupXpEvent pxp){
		EntityPlayer player = pxp.entityPlayer;
		EntityXPOrb orb = pxp.orb;
		int amount = orb.getXpValue();
		System.out.println("You have picked up " + amount + " XP! ");
		PlayerDataManager.addXPToPlayer(player, amount);
	}
	

}
