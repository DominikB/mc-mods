package com.db.basicskills;

import com.db.basicskills.events.BasicSkillsEventHandler;

import net.minecraft.init.Blocks;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;

@Mod(modid = BasicSkills.MODID, version = BasicSkills.VERSION)
/**
 * A MOD which adds a skill system to the game. This MOD has no skill contend.
 * It is just a library for other MODs using this, to have the same system.
 * 
 * @author Dominik Bruehl
 * @version 1.0
 * @date 16.02.2015
 *
 */
public class BasicSkills
{
	/** The MOD id. */
    public static final String MODID = "db_basicskills";
    /** The version of the MOD. */
    public static final String VERSION = "1.0";
    
    private BasicSkillsEventHandler bsEventHandler;
    
    public BasicSkills() {
    	bsEventHandler = new BasicSkillsEventHandler();
	}
    
    @EventHandler
    /**
     * The Main initialization.
     * @param event - The FMLInitializationEvent.
     */
    public void init(FMLInitializationEvent event)
    {
    	PlayerDataManager.loadPlayerData();
    	FMLCommonHandler.instance().bus().register(bsEventHandler);
    	MinecraftForge.EVENT_BUS.register(bsEventHandler);
    }
    
	@EventHandler
	public void saveData(FMLServerStoppingEvent event)
	{
		System.out.println("TEST\n\n\n\n");
		PlayerDataManager.savePlayerData();
		System.out.println("Data saved! ");
	}
}
